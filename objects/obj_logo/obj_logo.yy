{
    "id": "eda99d59-5c00-44ed-8891-3a6d39fd4695",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_logo",
    "eventList": [
        {
            "id": "30c96478-64d1-4a87-a611-cd9ad7f313ea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "eda99d59-5c00-44ed-8891-3a6d39fd4695"
        },
        {
            "id": "58063509-efed-4489-9a08-82222ef12fae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eda99d59-5c00-44ed-8891-3a6d39fd4695"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d1955324-806e-42a4-8918-6d72dfb11000",
    "visible": true
}