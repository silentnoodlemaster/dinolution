{
    "id": "3c0c995e-6e6c-411f-aaa2-7589488d3d47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_saur",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64caea0d-b43d-4582-9a84-fc7eefa4dac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c0c995e-6e6c-411f-aaa2-7589488d3d47",
            "compositeImage": {
                "id": "31ec4472-42a3-47c9-9668-118a169b9978",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64caea0d-b43d-4582-9a84-fc7eefa4dac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9d6a4c-add9-4122-a5c8-c8b2d082ba24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64caea0d-b43d-4582-9a84-fc7eefa4dac6",
                    "LayerId": "cc6e321e-7ecc-4552-b6d9-9a3c9b49f9ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cc6e321e-7ecc-4552-b6d9-9a3c9b49f9ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c0c995e-6e6c-411f-aaa2-7589488d3d47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 42,
    "yorig": 29
}