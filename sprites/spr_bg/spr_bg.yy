{
    "id": "79fb3497-518a-47ee-a1f2-75a45d24c4f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c9cc387-ba7d-4bfe-9948-eb823a049eca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79fb3497-518a-47ee-a1f2-75a45d24c4f8",
            "compositeImage": {
                "id": "e07cf4a4-77fd-4dd8-b3f3-8fed732f4bb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9cc387-ba7d-4bfe-9948-eb823a049eca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecb0f4f-08a5-4ca9-9eb6-e2ebd8355cc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9cc387-ba7d-4bfe-9948-eb823a049eca",
                    "LayerId": "e98a59db-4d3a-4983-85e9-33621db7fc97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e98a59db-4d3a-4983-85e9-33621db7fc97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79fb3497-518a-47ee-a1f2-75a45d24c4f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}