{
    "id": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f5f7d75-c773-4177-b1da-33b9cbd75710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
            "compositeImage": {
                "id": "64a5460b-6779-4466-a91b-848d17343cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f5f7d75-c773-4177-b1da-33b9cbd75710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a66d1c3-75a7-43ab-ac18-ba8864c92dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f5f7d75-c773-4177-b1da-33b9cbd75710",
                    "LayerId": "480ef9aa-cc75-446d-a388-e7cb49365e61"
                }
            ]
        },
        {
            "id": "1b590dc0-7e82-47d3-a536-62423969fa69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
            "compositeImage": {
                "id": "ced59236-6be4-49d3-82ef-320d33e17028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b590dc0-7e82-47d3-a536-62423969fa69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8fc2cfc-a328-4e05-8e67-5fb96213fb64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b590dc0-7e82-47d3-a536-62423969fa69",
                    "LayerId": "480ef9aa-cc75-446d-a388-e7cb49365e61"
                }
            ]
        },
        {
            "id": "007638ca-77b5-4fce-838e-4cbb96a5e303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
            "compositeImage": {
                "id": "13310e95-0cf3-4d5f-a2a5-2f3dd834ed2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007638ca-77b5-4fce-838e-4cbb96a5e303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd999d4f-8c86-4180-b86e-991d16adc49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007638ca-77b5-4fce-838e-4cbb96a5e303",
                    "LayerId": "480ef9aa-cc75-446d-a388-e7cb49365e61"
                }
            ]
        },
        {
            "id": "6700cd32-1d9d-41c6-8904-85c3ef7b3a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
            "compositeImage": {
                "id": "102dab65-e32d-40ac-8529-fc06ae128646",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6700cd32-1d9d-41c6-8904-85c3ef7b3a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bb61f95-1c8f-4491-b03c-b24c2cf5a125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6700cd32-1d9d-41c6-8904-85c3ef7b3a5f",
                    "LayerId": "480ef9aa-cc75-446d-a388-e7cb49365e61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "480ef9aa-cc75-446d-a388-e7cb49365e61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}