{
    "id": "268d8ed8-4e40-42b0-ad8a-0f22f28ecac4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energyplus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 11,
    "bbox_right": 51,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8db2471a-7608-4957-bc64-c11af5c8d60a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "268d8ed8-4e40-42b0-ad8a-0f22f28ecac4",
            "compositeImage": {
                "id": "1bec19c6-03d3-4886-ac86-57dd66af3bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db2471a-7608-4957-bc64-c11af5c8d60a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3c8eed-9429-4709-8a7e-784479781e60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db2471a-7608-4957-bc64-c11af5c8d60a",
                    "LayerId": "673e65c8-5ddb-4d76-a2bd-330e0243e48d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "673e65c8-5ddb-4d76-a2bd-330e0243e48d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "268d8ed8-4e40-42b0-ad8a-0f22f28ecac4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}