{
    "id": "d1955324-806e-42a4-8918-6d72dfb11000",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 3,
    "bbox_right": 250,
    "bbox_top": 29,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4935e8c5-1f77-4f31-a4e8-26737a53a088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1955324-806e-42a4-8918-6d72dfb11000",
            "compositeImage": {
                "id": "b66e5d37-beab-4c3e-9f73-844723665837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4935e8c5-1f77-4f31-a4e8-26737a53a088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba7ee0bf-b631-4693-9109-cdb22964027b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4935e8c5-1f77-4f31-a4e8-26737a53a088",
                    "LayerId": "43cc737c-d740-4823-9da8-c3658b9a1ace"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "43cc737c-d740-4823-9da8-c3658b9a1ace",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1955324-806e-42a4-8918-6d72dfb11000",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 127
}