draw_self();
if(positive)
{
	draw_set_color(c_aqua);
}
else
{
	draw_set_color(c_red);
}
draw_set_alpha(alpha);
draw_text(x,y, string(amount));
draw_set_alpha(1);
draw_set_color(c_white);