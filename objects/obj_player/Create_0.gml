global.win = false;
MAX_FALL = 10;
JMP_SPD = 19;
MAX_SPD = 16;
MIN_SPD = 4;
GRAV = 1.5;
FALL_GRAV = 3;

cmr_offset = 64;
energy_level = 55;
hspd = 0;
vspd = 0;
jmp_ready = true;
jmp_cancel = false;
dash_immunity = false;
dash_ready = true;

alarm[0] = 5;