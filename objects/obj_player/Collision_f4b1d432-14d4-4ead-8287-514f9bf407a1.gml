/// @description Insert description here
// You can write your code in this editor
if (!dash_immunity)
{
	if (energy_level < 25)
	{
		energy_level = 0;
	}
	else
	{
		energy_level -= 25;
	}
	with(other)
	{
		instance_destroy();
		effect_create_above(ef_firework, x, y, 0.5, c_red);
		minus = instance_create_layer(x, y, "Instances", obj_energyeventdisplay);
		minus.positive = false;
		minus.amount = 25;
	}
}