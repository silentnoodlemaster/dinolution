{
    "id": "2e1c625b-811d-457a-bcec-c422dc644689",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d98b925-9e92-41a5-a1a0-c4e6f1e731d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e1c625b-811d-457a-bcec-c422dc644689",
            "compositeImage": {
                "id": "fb1482d2-99a1-4d40-b00c-b47e77f62f30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d98b925-9e92-41a5-a1a0-c4e6f1e731d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b855c56-1404-4267-9f7c-c97f8a926d0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d98b925-9e92-41a5-a1a0-c4e6f1e731d9",
                    "LayerId": "452ae787-b0da-4dfc-a729-64d6bbc9870b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "452ae787-b0da-4dfc-a729-64d6bbc9870b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e1c625b-811d-457a-bcec-c422dc644689",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}