{
    "id": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 7,
    "bbox_right": 119,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab4572ef-7967-4a95-8f30-506ccb13a373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "1193528f-9bdb-4a6f-9c3b-4b217e9bef38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab4572ef-7967-4a95-8f30-506ccb13a373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb996912-04a0-4d3d-a8e6-14080657f9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab4572ef-7967-4a95-8f30-506ccb13a373",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "2c0cdd68-f870-4619-9766-1fec6d023b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "30e8fb85-47e6-411e-950f-ec2cc8a05911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0cdd68-f870-4619-9766-1fec6d023b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e99c46f-3f6e-400b-8b8e-b61df70c38a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0cdd68-f870-4619-9766-1fec6d023b31",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "2dc559df-437d-416c-81ba-9a63e5f5103a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "444bb55b-eb33-44cf-b70a-a8b8ae31b593",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dc559df-437d-416c-81ba-9a63e5f5103a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19346492-151d-4db3-b0f8-8dacaf9ccf91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dc559df-437d-416c-81ba-9a63e5f5103a",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "225feb22-5be9-4bbd-b947-3c8255ca53e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "9fda1fae-f5ec-431a-953a-256d876f80fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "225feb22-5be9-4bbd-b947-3c8255ca53e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d30759e-ea20-4ee2-b9ba-99c340f0032c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "225feb22-5be9-4bbd-b947-3c8255ca53e1",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "d212f0fa-2cdf-4ea2-9a66-1cac4a30ba89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "7738f67b-9a5e-4c68-bb97-2188e796e92c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d212f0fa-2cdf-4ea2-9a66-1cac4a30ba89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d99a95-ae42-420a-9a82-6129ee36bce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d212f0fa-2cdf-4ea2-9a66-1cac4a30ba89",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "3202c4d0-cf4d-4067-b430-a077ee4d4247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "8ed091ac-0e1c-494f-9c50-5c2cea59fc40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3202c4d0-cf4d-4067-b430-a077ee4d4247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c04310-139a-45f5-8c15-d6635b056087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3202c4d0-cf4d-4067-b430-a077ee4d4247",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "881b04a6-8923-463b-8dd6-efa6062eb198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "bb6b2041-07d1-4184-ac5f-5dd2336492f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "881b04a6-8923-463b-8dd6-efa6062eb198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b984e25-2442-4a7e-b317-52f20b746716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "881b04a6-8923-463b-8dd6-efa6062eb198",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "005fbd44-f022-4e2d-9c2f-82916fcbf051",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "7877fbcd-75bb-4591-8a25-c7c715ece297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005fbd44-f022-4e2d-9c2f-82916fcbf051",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95536d03-b87a-43b5-8145-9d115a2b6102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005fbd44-f022-4e2d-9c2f-82916fcbf051",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        },
        {
            "id": "30a82321-fdcd-4bac-a0f6-f3adcf276ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "compositeImage": {
                "id": "d23d478a-baab-47b0-8e8e-4033c3e1bb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30a82321-fdcd-4bac-a0f6-f3adcf276ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5abf4a07-04d0-4d34-a7d7-759f5182d274",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30a82321-fdcd-4bac-a0f6-f3adcf276ddc",
                    "LayerId": "59c81b59-508c-4785-a52e-6606ad8f5853"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "59c81b59-508c-4785-a52e-6606ad8f5853",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}