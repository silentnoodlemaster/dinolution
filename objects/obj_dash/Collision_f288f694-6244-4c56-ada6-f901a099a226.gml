/// @description Insert description here
// You can write your code in this editor
with(other)
{
	instance_destroy();
	effect_create_above(ef_firework, x, y, 0.5, c_red);
	plus = instance_create_layer(x, y, "Instances", obj_energyeventdisplay);
	plus.positive = true;
	plus.amount = energy_value;
	if (obj_player.energy_level <= 100 - energy_value)
	{
		obj_player.energy_level += energy_value;
	}
	else if (obj_player.energy_level < 100)
	{
		obj_player.energy_level = 100;
	}
}