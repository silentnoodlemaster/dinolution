{
    "id": "b92ac1e4-589f-4efe-8ca5-a8dc00604519",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_saur",
    "eventList": [
        {
            "id": "86f13ab8-6bd3-4c03-8290-c4e480e88968",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b92ac1e4-589f-4efe-8ca5-a8dc00604519"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "23f1c961-b6b2-4392-b15e-9d5885a34181",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "afa4bda2-2c5d-4275-b94e-2e72b0292849",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 0
        },
        {
            "id": "b3686eb4-4322-4130-a25e-e00ae63c7111",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 128,
            "y": 64
        },
        {
            "id": "61e2133d-5856-40a0-a982-da2a8b05f2ff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3c0c995e-6e6c-411f-aaa2-7589488d3d47",
    "visible": true
}