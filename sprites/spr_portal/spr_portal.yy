{
    "id": "47707351-96fb-4850-a153-12cf99948dab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_portal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 19,
    "bbox_right": 43,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4b07866-fbbf-421d-865b-da4297c62514",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47707351-96fb-4850-a153-12cf99948dab",
            "compositeImage": {
                "id": "1882c231-76d1-490e-8186-6ddde07a62e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4b07866-fbbf-421d-865b-da4297c62514",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63e2483-b8da-4517-b617-ee10ed4455ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4b07866-fbbf-421d-865b-da4297c62514",
                    "LayerId": "e7944bcb-8d9d-4f7c-a056-05e55faa300f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7944bcb-8d9d-4f7c-a056-05e55faa300f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47707351-96fb-4850-a153-12cf99948dab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}