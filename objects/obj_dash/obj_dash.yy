{
    "id": "8f8d522b-4a92-42d6-9cf1-34e8d674bf1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dash",
    "eventList": [
        {
            "id": "828ddc3b-162c-448d-8839-ae17c725b5bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f8d522b-4a92-42d6-9cf1-34e8d674bf1e"
        },
        {
            "id": "e7010137-40fe-4f10-85c1-7b4cafceea23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f8d522b-4a92-42d6-9cf1-34e8d674bf1e"
        },
        {
            "id": "f288f694-6244-4c56-ada6-f901a099a226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b92ac1e4-589f-4efe-8ca5-a8dc00604519",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8f8d522b-4a92-42d6-9cf1-34e8d674bf1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1a042530-178d-4b1d-b107-ea2c33b027de",
    "visible": true
}