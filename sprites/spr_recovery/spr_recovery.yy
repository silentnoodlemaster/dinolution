{
    "id": "149324b7-b115-4c4b-b5d4-ebf3a6378966",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_recovery",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a65dd9e-7e6c-4a28-b164-6e8e4b8d4967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "149324b7-b115-4c4b-b5d4-ebf3a6378966",
            "compositeImage": {
                "id": "6859484f-30e3-474f-bd18-39bb7f7f1292",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a65dd9e-7e6c-4a28-b164-6e8e4b8d4967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02654087-10e8-44ec-87d8-dd1e82405586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a65dd9e-7e6c-4a28-b164-6e8e4b8d4967",
                    "LayerId": "b23f197a-129e-47cb-b552-a0277071baad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b23f197a-129e-47cb-b552-a0277071baad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "149324b7-b115-4c4b-b5d4-ebf3a6378966",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}