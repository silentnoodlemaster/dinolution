{
    "id": "ca55e329-2a7f-4643-a664-b4352a1cc593",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_floor",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "fc2a71be-d8f5-4c3e-b26e-28dfdbcf1580",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "ded5139f-b930-4893-ad78-ac14b5c9812a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 0
        },
        {
            "id": "744b2e1e-a390-479c-9b6e-d6805f2ed69c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 16,
            "y": 16
        },
        {
            "id": "16ee6c0a-ab6e-447a-9cd8-8bc8aa785bdf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 16
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "9be0a154-3627-4716-8e5a-c67e0e0f994f",
    "visible": true
}