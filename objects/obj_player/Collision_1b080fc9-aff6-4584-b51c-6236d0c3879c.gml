if (!dash_immunity)
{
	if (energy_level > 0)
	{
		energy_level = 0;
	}
	
	with(other)
	{
		instance_destroy();
		effect_create_above(ef_firework, x, y, 0.5, c_red);
		minus = instance_create_layer(x, y, "Instances", obj_energyeventdisplay);
		minus.positive = false;
		minus.amount = 100;
		
		room_goto_next()
	}
}
if (dash_immunity)
{
	if (energy_level > 0)
	{
		energy_level = 0;
	}
	
	with(other)
	{
		instance_destroy();
		effect_create_above(ef_firework, x, y, 0.5, c_red);
		minus = instance_create_layer(x, y, "Instances", obj_energyeventdisplay);
		minus.positive = false;
		minus.amount = 100;
		
		room_goto_next()
	}
}


