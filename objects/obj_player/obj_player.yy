{
    "id": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "32fb928e-2428-4ec0-8c82-e9344dc95122",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "18163101-c618-4070-8bc8-8120f3e053a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "5f14f3ff-f96d-44d7-abb6-0db1f986808e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ca55e329-2a7f-4643-a664-b4352a1cc593",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "5e4dd987-4308-4cc3-801b-59d23f785951",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "d60a9481-4687-4b76-bf89-347fcff53a7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "b3f8fc76-65a6-4f33-b1cd-20de44191902",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "f4b1d432-14d4-4ead-8287-514f9bf407a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b92ac1e4-589f-4efe-8ca5-a8dc00604519",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "1b080fc9-aff6-4584-b51c-6236d0c3879c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5df17b4d-1b2e-40a3-9d91-1c43a46b4358",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "05c7bada-b8a1-46d3-9b3a-e21606ec8ca2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        },
        {
            "id": "ec8a5fa9-706e-43cd-b869-ecfd08243410",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4d13c754-c3dc-4b20-a48f-9acb85bc26aa",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9cc72e5b-d0a9-462d-a40d-7a21a9c2013b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "e2b60fde-b96c-4a07-a441-1d9f38abb715",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "d881ff71-eaa6-4d40-8794-e4edd6aaa00c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "13e247d8-229d-43b6-8e36-ff7880291ca4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "241ed1a7-a663-4b13-9db0-f02c8494a4f8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4cf3a9d-1846-4744-b718-b4cf2d051751",
    "visible": true
}