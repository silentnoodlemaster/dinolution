if (place_meeting(x,y+vspd,obj_floor))
{
    //When it's not 1 pixel a way from the wall it moves closer to the wall
    while(!place_meeting(x,y+sign(vspd),obj_floor))
    {
        y = y + sign(vspd);
    }
    vspd = 0;
	hspd = 0; // TODO energy level
	effect_create_above(ef_smoke, x, y+sprite_height/2, 2, c_ltgray);
}

if (sprite_index != spr_recovery)
{
	x += hspd;
	y += vspd;
}
else 
{
	y = obj_player.y;
}

if(x <= obj_player.x && vspd == 0){
	obj_player.visible = true;
	obj_player.image_index = spr_player;
	obj_player.y = y;
	obj_player.dash_immunity = false;
	obj_player.dash_ready = true;
	obj_player.jmp_ready = true;
	instance_destroy();
}