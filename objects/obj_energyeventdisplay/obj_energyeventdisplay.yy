{
    "id": "0d3cdcca-f8eb-4d43-82b9-1ce0c93e42f7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_energyeventdisplay",
    "eventList": [
        {
            "id": "03e2e0bf-5b6b-43ff-aefd-6c225f97dff0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0d3cdcca-f8eb-4d43-82b9-1ce0c93e42f7"
        },
        {
            "id": "8653db20-0552-4a67-882f-0f91cacf2ed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0d3cdcca-f8eb-4d43-82b9-1ce0c93e42f7"
        },
        {
            "id": "6b3875e3-b3d4-4d1f-9036-29a7e7762927",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0d3cdcca-f8eb-4d43-82b9-1ce0c93e42f7"
        },
        {
            "id": "c6a57f43-ba08-4ed8-a889-e8f14ed5bc59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0d3cdcca-f8eb-4d43-82b9-1ce0c93e42f7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "268d8ed8-4e40-42b0-ad8a-0f22f28ecac4",
    "visible": true
}