{
    "id": "5d57f95c-f41c-4b7e-b5aa-ea8dd03f7e52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_energyminus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 11,
    "bbox_right": 51,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "faa2ba41-791c-4142-9bae-f3c3a889c728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d57f95c-f41c-4b7e-b5aa-ea8dd03f7e52",
            "compositeImage": {
                "id": "4bfd58fc-1eda-4376-830b-634418b3f370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa2ba41-791c-4142-9bae-f3c3a889c728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "250cc954-ed24-426a-8ca7-c32b6216afc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa2ba41-791c-4142-9bae-f3c3a889c728",
                    "LayerId": "9e48f457-8840-4256-81fa-482a9885fa2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e48f457-8840-4256-81fa-482a9885fa2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d57f95c-f41c-4b7e-b5aa-ea8dd03f7e52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}