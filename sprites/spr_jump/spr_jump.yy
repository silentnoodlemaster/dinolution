{
    "id": "6e7c3ee9-ea66-4583-9677-0ee5025587c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb53e59d-7dd1-41ef-a018-69c4ec225c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e7c3ee9-ea66-4583-9677-0ee5025587c9",
            "compositeImage": {
                "id": "db6d89cd-b0e3-46c8-9400-5e67aab62697",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb53e59d-7dd1-41ef-a018-69c4ec225c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40254a05-a54d-4e8b-a85e-d4e57d888978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb53e59d-7dd1-41ef-a018-69c4ec225c73",
                    "LayerId": "dcfe3f85-c12e-48ea-ad71-7a743b293789"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dcfe3f85-c12e-48ea-ad71-7a743b293789",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e7c3ee9-ea66-4583-9677-0ee5025587c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}