/// @description Insert description here
// You can write your code in this editor
energy_index = floor(obj_player.energy_level * 0.1)-1;

if (energy_index > -1)
{
	sprite_index = spr_energy;
	image_speed = 0;
	if (energy_index >= 8)
	{
		image_index = 8;
	}
	else
	{
		image_index = energy_index;
	}
}
else
{
	sprite_index = spr_lowenergy;
	image_speed = 1;
}

x = camera_get_view_x(view_camera[0]);