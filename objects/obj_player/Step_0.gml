bg = layer_get_id("Background");
layer_hspeed(bg,hspd/2);

camera_set_view_pos(view_camera[0],x-cmr_offset,0);

if (energy_level > 0)
{
	hspd = ((MAX_SPD - MIN_SPD) * (energy_level * 0.01)) + MIN_SPD;
}

else
{
	hspd = 0;
	sprite_index = spr_recovery;
}
if ( energy_level <=0) {room_goto_next();}


key_jump = keyboard_check_pressed(vk_space);
key_jump_release = keyboard_check_released(vk_space);

if (vspd < 0 && !place_meeting(x,y+1,obj_floor)) 
{
	vspd += GRAV;
}
else if (!place_meeting(x,y+1,obj_floor))
{
	vspd += FALL_GRAV;
}

if (place_meeting(x,y+1,obj_floor) && jmp_ready && energy_level > 0)
{
    vspd = key_jump * -JMP_SPD;
	jmp_cancel = false;
	sprite_index = spr_player;
	/*if (key_jump){
		sprite_index = spr_jump;
	}*/
}
if (!place_meeting(x,y+1,obj_floor))
{
	sprite_index = spr_jump;
}
// variable jump height, when you release your jump upward momentum gets halved
if(!place_meeting(x,y+1,obj_floor) && keyboard_check_released(vk_space) && !jmp_cancel && vspd<0){
	vspd = vspd/2;
	jmp_cancel = true;
}
if (!place_meeting(x,y+1,obj_floor) && keyboard_check_pressed(vk_space) && dash_ready && energy_level > 0)
{
	jmp_ready = false;
	dash_ready = false;
	visible = false;
	dash_immunity = true;
	instance_create_layer(x,y,layer,obj_dash);
}


if (place_meeting(x,y+vspd,obj_floor))
{
    //When it's not 1 pixel a way from the wall it moves closer to the wall
    while(!place_meeting(x,y+sign(vspd),obj_floor))
    {
        y = y + sign(vspd);
    }
    vspd = 0;
	if (instance_exists(obj_dash))
	{
		obj_dash.sprite_index = spr_recovery;
	}
	jmp_ready = true;
}



obj_energy.x += hspd;
x += hspd;
y += vspd;


