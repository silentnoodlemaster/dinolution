{
    "id": "797e73c2-3553-49ab-921b-3f0c1390dbd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ce106c66-571f-4267-9f19-aa53b40d1555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "797e73c2-3553-49ab-921b-3f0c1390dbd2",
            "compositeImage": {
                "id": "109cb96a-aeca-4f19-8c8a-d6e4ff8060a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce106c66-571f-4267-9f19-aa53b40d1555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac91ac5-8c80-4489-95e6-6ae0a55c649e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce106c66-571f-4267-9f19-aa53b40d1555",
                    "LayerId": "7b5a8766-3cf4-4e83-bc7b-b5279018a881"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7b5a8766-3cf4-4e83-bc7b-b5279018a881",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "797e73c2-3553-49ab-921b-3f0c1390dbd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}