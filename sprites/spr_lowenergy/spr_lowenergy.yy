{
    "id": "159a9254-f4ab-4b1c-b596-e04a81feb0e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lowenergy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 7,
    "bbox_right": 119,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af36baeb-92c8-4ec2-9ff4-5165e8e7dedf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9254-f4ab-4b1c-b596-e04a81feb0e1",
            "compositeImage": {
                "id": "5e1d6b9d-3e93-4c00-a038-7f788781e10a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af36baeb-92c8-4ec2-9ff4-5165e8e7dedf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961bdffd-5499-446a-a547-931c608b21cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af36baeb-92c8-4ec2-9ff4-5165e8e7dedf",
                    "LayerId": "8c1d60c8-7595-46eb-bba1-7aae30dff98b"
                }
            ]
        },
        {
            "id": "cf686f48-b628-4501-be85-b64ee9c34503",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9254-f4ab-4b1c-b596-e04a81feb0e1",
            "compositeImage": {
                "id": "6df83fde-9c96-49fc-9e2e-21f822f2f013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf686f48-b628-4501-be85-b64ee9c34503",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e228541-d59e-4fbc-9bf6-bc6f85d9cccc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf686f48-b628-4501-be85-b64ee9c34503",
                    "LayerId": "8c1d60c8-7595-46eb-bba1-7aae30dff98b"
                }
            ]
        },
        {
            "id": "313159e9-3f49-462b-aaf4-0024d3ad67e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9254-f4ab-4b1c-b596-e04a81feb0e1",
            "compositeImage": {
                "id": "3ee4cb31-ea84-4c8f-9f10-7f5a2031d404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313159e9-3f49-462b-aaf4-0024d3ad67e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "289cc4ab-1a3c-4b55-b459-484285934e22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313159e9-3f49-462b-aaf4-0024d3ad67e8",
                    "LayerId": "8c1d60c8-7595-46eb-bba1-7aae30dff98b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8c1d60c8-7595-46eb-bba1-7aae30dff98b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "159a9254-f4ab-4b1c-b596-e04a81feb0e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}