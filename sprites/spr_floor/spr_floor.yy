{
    "id": "9be0a154-3627-4716-8e5a-c67e0e0f994f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d432bf41-8e90-42b7-9148-a92130b5949f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9be0a154-3627-4716-8e5a-c67e0e0f994f",
            "compositeImage": {
                "id": "edf0bfc5-c716-49b2-b2b6-2d0c53cdf90b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d432bf41-8e90-42b7-9148-a92130b5949f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afcf6830-79fa-452c-98b5-2bb642d1b33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d432bf41-8e90-42b7-9148-a92130b5949f",
                    "LayerId": "5d6f8a68-c43a-4afb-b630-48b1d8bfef34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5d6f8a68-c43a-4afb-b630-48b1d8bfef34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9be0a154-3627-4716-8e5a-c67e0e0f994f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}