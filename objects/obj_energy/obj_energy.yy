{
    "id": "78e38ebc-c5fd-4a13-acbd-ef826eebe579",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_energy",
    "eventList": [
        {
            "id": "ac24f05e-5aa1-4159-9155-83fc60d1766c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "78e38ebc-c5fd-4a13-acbd-ef826eebe579"
        },
        {
            "id": "e61661ef-c6fc-4ae1-8307-0cd43457ee13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "78e38ebc-c5fd-4a13-acbd-ef826eebe579"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "37ced4e9-d10a-4390-95c0-3d4871de24f3",
    "visible": true
}