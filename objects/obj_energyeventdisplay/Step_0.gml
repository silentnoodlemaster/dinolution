if(positive)
{
	sprite_index = spr_energyplus;
}
if(!positive)
{
	sprite_index = spr_energyminus;
}

if (fading)
{
	alpha -= 0.05;
}

image_alpha = alpha;

if (alpha <= 0)
{
	instance_destroy();
	image_alpha = 1;
}

x = obj_player.x;
y -= 3;