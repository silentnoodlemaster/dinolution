{
    "id": "1f0701b8-bd0b-4dcb-8b7f-b016348616d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ggm8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bc76358-7716-43fa-b7c0-a774152176a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f0701b8-bd0b-4dcb-8b7f-b016348616d4",
            "compositeImage": {
                "id": "e17f9ae8-a991-4d1e-a13b-68078e85bd74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc76358-7716-43fa-b7c0-a774152176a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ac6421c-7e99-4c1b-93cb-5240dc5f6163",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc76358-7716-43fa-b7c0-a774152176a9",
                    "LayerId": "81ae064a-96f5-4bb3-9744-04eb462743b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81ae064a-96f5-4bb3-9744-04eb462743b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f0701b8-bd0b-4dcb-8b7f-b016348616d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}