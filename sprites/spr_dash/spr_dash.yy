{
    "id": "1a042530-178d-4b1d-b107-ea2c33b027de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d576b46-8d17-4ad6-a7c2-79a644050018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a042530-178d-4b1d-b107-ea2c33b027de",
            "compositeImage": {
                "id": "718f0af5-609f-4037-95c7-6770ddd4ac78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d576b46-8d17-4ad6-a7c2-79a644050018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd5087e-beba-4249-94f3-fd257c431cb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d576b46-8d17-4ad6-a7c2-79a644050018",
                    "LayerId": "787a0e03-5f46-4071-8277-c28389100d2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "787a0e03-5f46-4071-8277-c28389100d2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a042530-178d-4b1d-b107-ea2c33b027de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}