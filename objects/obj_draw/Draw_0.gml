if (room_get_name(room) == "rm_end") {
	draw_set_halign(fa_center);
	draw_set_font(fnt_arial_no_aa);
	draw_set_color(c_silver);
	if(global.win) {
		draw_text(room_width/2,room_height/4,"YOU WIN!");
	}
	else {
		draw_text(room_width/2,room_height/4,"YOU LOSE!");
	}
	draw_text(room_width/2,room_height/2,"TO PLAY AGAIN PRESS SPACE");
	draw_set_halign(fa_left);
	draw_set_color(c_white);
}
